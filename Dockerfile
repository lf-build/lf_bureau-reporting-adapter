FROM registry.lendfoundry.com/base:beta8

ADD ./global.json /app/

ADD ./src/LendFoundry.BureauReporting.Adapter.Abstractions /app/LendFoundry.BureauReporting.Adapter.Abstractions
WORKDIR /app/LendFoundry.BureauReporting.Adapter.Abstractions
#RUN dnu restore --ignore-failed-sources --no-cache -s http://107.170.250.178/guestAuth/app/nuget/v1/FeedService.svc/ --no-cache -s https://www.nuget.org/api/v2/ --no-cache -s https://www.myget.org/F/aspnetvnext/api/v2/
RUN eval "$CMD_RESTORE"
RUN dnu build

ADD ./src/LendFoundry.BureauReporting.Adapter.Persistence /app/LendFoundry.BureauReporting.Adapter.Persistence
WORKDIR /app/LendFoundry.BureauReporting.Adapter.Persistence
#RUN dnu restore --ignore-failed-sources --no-cache -s http://107.170.250.178/guestAuth/app/nuget/v1/FeedService.svc/ --no-cache -s https://www.nuget.org/api/v2/ --no-cache -s https://www.myget.org/F/aspnetvnext/api/v2/
RUN eval "$CMD_RESTORE"
RUN dnu build

ADD ./src/LendFoundry.BureauReporting.Adapter /app/LendFoundry.BureauReporting.Adapter
WORKDIR /app/LendFoundry.BureauReporting.Adapter
#RUN dnu restore --ignore-failed-sources --no-cache -s http://107.170.250.178/guestAuth/app/nuget/v1/FeedService.svc/ --no-cache -s https://www.nuget.org/api/v2/ --no-cache -s https://www.myget.org/F/aspnetvnext/api/v2/
RUN eval "$CMD_RESTORE"
RUN dnu build

ADD ./src/LendFoundry.BureauReporting.Adapter.Api /app/LendFoundry.BureauReporting.Adapter.Api
WORKDIR /app/LendFoundry.BureauReporting.Adapter.Api
#RUN dnu restore --ignore-failed-sources --no-cache -s http://107.170.250.178/guestAuth/app/nuget/v1/FeedService.svc/ --no-cache -s https://www.nuget.org/api/v2/ --no-cache -s https://www.myget.org/F/aspnetvnext/api/v2/
RUN eval "$CMD_RESTORE"
RUN dnu build

EXPOSE 5000
ENTRYPOINT dnx kestrel