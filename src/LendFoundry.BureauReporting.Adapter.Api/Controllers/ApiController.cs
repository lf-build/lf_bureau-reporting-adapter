﻿using LendFoundry.BureauReporting.Abstractions.Requests;
using LendFoundry.BureauReporting.Adapter.Abstractions;
using LendFoundry.Foundation.Logging;
using LendFoundry.Foundation.Services;
using LendFoundry.Notifications.Client;
using LendFoundry.Security.Tokens;
using LendFoundry.Tenant.Client;
using Microsoft.AspNet.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;

// For more information on enabling Web API for empty projects, visit http://go.microsoft.com/fwlink/?LinkID=397860

namespace LendFoundry.BureauReporting.Adapter.Api.Controllers
{
    [Route("/")]
    public class ApiController : Controller
    {
        private IAdapterService Service { get; set; }
        private INotificationServiceFactory NotificationServiceFactory { get; set; }
        private ITokenReaderFactory TokenReaderFactory { get; set; }
        private ITenantService TenantService { get; set; }
        private ILogger Logger { get; set; }

        public ApiController(IAdapterService service, ILogger logger,
            INotificationServiceFactory notificationServiceFactory,
            ITokenReaderFactory tokenReaderFactory,
            ITenantService tenantService)
        {
            Service = service;
            Logger = logger;
            NotificationServiceFactory = notificationServiceFactory;
            TokenReaderFactory = tokenReaderFactory;
            TenantService = tenantService;
        }

        [HttpGet("/getAllBureauReportData")]
        public IActionResult GetAllBureauReportData()
        {
            return Execute(() =>
            {
                var bureauReportData = Service.GetCurrentPeriodData();
                if (bureauReportData.Any())
                {
                    return new HttpOkObjectResult(bureauReportData);
                }
                throw new NotFoundException("data not found");
            });
        }

        [HttpGet("/getBureauReportData/{loanReferenceNumber}")]
        public IActionResult GetBureauReportData(string loanReferenceNumber)
        {
            return Execute(() =>
            {
                var bureauReportData = Service.GetBureauReportData(loanReferenceNumber);
                if (bureauReportData != null)
                {
                    return new HttpOkObjectResult(bureauReportData);
                }
                throw new NotFoundException("data not found");
            });
        }

        [HttpGet("/addBureauReportData/{reportDate}")]
        public IActionResult AddBureauReportData(DateTime reportDate)
        {
            return null;
        }

        [HttpGet("/translateBureauReportData/{reportDate}")]
        public IActionResult TranslateBureauReportData(DateTime reportDate)
        {
            return null;
        }

        [HttpGet("/uploadBureauReportData/{reportDate}")]
        public IActionResult UploadBureauReportData(DateTime reportDate)
        {
            return null;
        }

        private IActionResult Execute(Func<IActionResult> expression)
        {
            try
            {
                return expression();
            }
            catch (Exception ex)
            {
                Logger.Error("Error when handling request:", ex);
                return ErrorResult.InternalServerError("We couldn't process your request");
            }
        }
    }
}
