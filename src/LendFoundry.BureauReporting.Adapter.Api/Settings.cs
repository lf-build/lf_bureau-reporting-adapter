﻿using LendFoundry.Foundation.Services.Settings;
using System;

namespace LendFoundry.BureauReporting.Adapter.Api
{
    public class Settings
    {
        private const string Prefix = "BUREAUREPORTING_ADAPTER";

        public static ServiceSettings EventHub { get; } = new ServiceSettings($"{Prefix}_EVENTHUB", "eventhub");

        public static ServiceSettings Configuration { get; } = new ServiceSettings($"{Prefix}_CONFIGURATION", "configuration");

        public static ServiceSettings Tenant { get; } = new ServiceSettings($"{Prefix}_TENANT", "tenant");

        public static ServiceSettings Loan { get; } = new ServiceSettings($"{Prefix}_LOAN", "loans");

        public static DatabaseSettings Mongo { get; } = new DatabaseSettings($"{Prefix}_MONGO_CONNECTION", "mongodb://mongo", $"{Prefix}_MONGO_DATABASE", "bureau-reporting-adapter");

        public static ServiceSettings BureauReporting { get; } = new ServiceSettings($"{Prefix}_BUREAUREPORTING", "bureaureporting");

        public static ServiceSettings Notification { get; } = new ServiceSettings($"{Prefix}_NOTIFICATION", "notifications");

        public static string ServiceName => Environment.GetEnvironmentVariable($"{Prefix}_TOKEN_ISSUER") ?? "bureau-reporting-adapter";
    }
}
