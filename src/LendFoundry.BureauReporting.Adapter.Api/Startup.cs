﻿using LendFoundry.BureauReporting.Adapter.Abstractions;
using LendFoundry.BureauReporting.Adapter.Abstractions.Configuration;
using LendFoundry.BureauReporting.Adapter.Persistence;
using LendFoundry.BureauReporting.Client;
using LendFoundry.Configuration.Client;
using LendFoundry.EventHub.Client;
using LendFoundry.Foundation.Date;
using LendFoundry.Foundation.Logging;
using LendFoundry.Foundation.Persistence.Mongo;
using LendFoundry.Foundation.Services;
using LendFoundry.Loans.Client;
using LendFoundry.Notifications.Client;
using LendFoundry.Security.Tokens;
using LendFoundry.Tenant.Client;
using Microsoft.AspNet.Builder;
using Microsoft.AspNet.Hosting;
using Microsoft.Framework.DependencyInjection;

namespace LendFoundry.BureauReporting.Adapter.Api
{
    public class Startup
    {
        public Startup(IHostingEnvironment env) { }

        public void ConfigureServices(IServiceCollection services)
        {
            // services
            services.AddTokenHandler();
            services.AddHttpServiceLogging(Settings.ServiceName);
            services.AddTenantTime();
            services.AddConfigurationService<AdapterConfiguration>(Settings.Configuration.Host, Settings.Configuration.Port, Settings.ServiceName);
            services.AddEventHub(Settings.EventHub.Host, Settings.EventHub.Port, Settings.ServiceName);
            services.AddTenantService(Settings.Tenant.Host, Settings.Tenant.Port);
            services.AddLoanService(Settings.Loan.Host, Settings.Loan.Port);
            services.AddReportDataService(Settings.BureauReporting.Host, Settings.BureauReporting.Port);
            services.AddNotificationService(Settings.Notification.Host, Settings.Notification.Port);

            // interface implements
            services.AddSingleton<IMongoConfiguration>(p => new MongoConfiguration(Settings.Mongo.ConnectionString, Settings.Mongo.Database));
            services.AddTransient<IAdapterRepository, AdapterRepository>();
            services.AddTransient<IAdapterService, AdapterService>();
            services.AddTransient<IAdapterRepositoryFactory, AdapterRepositoryFactory>();
            services.AddTransient<IAdapterServiceFactory, AdapterServiceFactory>();
            services.AddTransient<IAdapterListener, AdapterListener>();

            services.AddTransient<IJobDispatcher, JobDispatcher>();
            services.AddTransient<IScheduleExecutorFactory, ScheduleExecutorFactory>();

            // eventhub factory
            services.AddTransient(p =>
            {
                var currentTokenReader = p.GetService<ITokenReader>();
                var staticTokenReader = new StaticTokenReader(currentTokenReader.Read());
                return p.GetService<IEventHubClientFactory>().Create(staticTokenReader);
            });            

            // aspnet mvc related
            services.AddMvc().AddLendFoundryJsonOptions();
            services.AddCors();            
        }

        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            app.UseCors(env);
            app.UseErrorHandling();
            app.UseRequestLogging();
            app.UseAdapterListener();
            app.UseJobDispacher();
            app.UseMvc();
            app.UseEventHub();
        }
    }
}
