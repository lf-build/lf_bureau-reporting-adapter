﻿using LendFoundry.BureauReporting.Abstractions.Domain;
using LendFoundry.Foundation.Persistence;
using System;

namespace LendFoundry.BureauReporting.Adapter.Abstractions
{
    public interface IAdapterEntry : IAggregate, IReportDataRequest
    {
        DateTimeOffset CutOffPeriod { get; set; }

        bool IsProcessed { get; set; }

        bool IsSkipped { get; set; }
    }
}
