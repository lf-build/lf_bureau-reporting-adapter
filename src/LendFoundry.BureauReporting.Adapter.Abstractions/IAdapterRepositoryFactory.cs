﻿using LendFoundry.Security.Tokens;

namespace LendFoundry.BureauReporting.Adapter.Abstractions
{
    public interface IAdapterRepositoryFactory
    {
        IAdapterRepository Create(ITokenReader reader);
    }
}
