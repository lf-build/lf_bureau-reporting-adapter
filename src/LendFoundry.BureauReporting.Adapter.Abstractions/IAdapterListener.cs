﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LendFoundry.BureauReporting.Adapter.Abstractions
{
    public interface IAdapterListener
    {
        void Start();
    }
}
