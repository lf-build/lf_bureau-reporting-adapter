﻿using LendFoundry.Loans;
using System;
using System.Collections.Generic;

namespace LendFoundry.BureauReporting.Adapter.Abstractions
{
    public class LoanPayment : ILoanPayment
    {
        public int Year { get; set; }
        public int Month { get; set; }

        public double Amount { get; set; }
        public string Code { get; set; }
        public DateTimeOffset Date { get; set; }
        public string Description { get; set; }
        public string Id { get; set; }
        public string LoanReferenceNumber { get; set; }
        public string Notes { get; set; }
        public string PaymentId { get; set; }
        public DateTimeOffset Timestamp { get; set; }
    }
}
