﻿using LendFoundry.Foundation.Logging;
using LendFoundry.Security.Tokens;
using LendFoundry.Tenant.Client;

namespace LendFoundry.BureauReporting.Adapter.Abstractions
{
    public interface IAdapterServiceFactory
    {
        IAdapterServiceExtended Create(StaticTokenReader reader, ILogger logger);
        IAdapterServiceExtended Create(StaticTokenReader reader, ILogger logger, TenantInfo tenantInfo);
    }
}
