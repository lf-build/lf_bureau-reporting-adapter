﻿using LendFoundry.BureauReporting.Abstractions.Domain;
using LendFoundry.BureauReporting.Abstractions.Requests.Account;
using LendFoundry.BureauReporting.Abstractions.Requests.Borrower;
using LendFoundry.BureauReporting.Abstractions.Requests.Loan;
using LendFoundry.BureauReporting.Abstractions.Requests.Payments;
using System.Collections.Generic;

namespace LendFoundry.BureauReporting.Adapter.Abstractions
{
    public interface IReportDataRequest
    {
        AccountRequestData AccountInfo { get; set; }
        ICollection<BorrowerRequestData> Borrowers { get; set; }
        LoanRequestData LoanInfo { get; set; }
        ICollection<PaymentDetailsRequestData> PaymentHistory { get; set; }
    }
}
