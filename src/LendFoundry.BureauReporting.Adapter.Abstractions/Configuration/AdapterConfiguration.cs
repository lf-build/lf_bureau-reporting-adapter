﻿namespace LendFoundry.BureauReporting.Adapter.Abstractions.Configuration
{
    public class AdapterConfiguration
    {
        public EventMapping[] Events { get; set; }
        public string ReportingDateDayOfMonth { get; set; }
    }
}
