﻿namespace LendFoundry.BureauReporting.Adapter.Abstractions.Configuration
{
    public class EventMapping
    {
        public string Name { get; set; }
        public string LoanReferenceNumber { get; set; }        
    }
}
