﻿using LendFoundry.Foundation.Date;
using System;

namespace LendFoundry.BureauReporting.Adapter.Abstractions.Configuration
{
    public class ConfigurationKeys
    {
        private string _ReportingDateDayOfMonth = string.Empty;
        public string ReportingDateDayOfMonth
        {
            get
            {
                if (string.Equals(_ReportingDateDayOfMonth, BureauReporting.Abstractions.Configuration.Common.LastDayOfMonth, StringComparison.OrdinalIgnoreCase))
                {
                    _ReportingDateDayOfMonth = DateTime.DaysInMonth(TenantDateTime.DateTime.Year, TenantDateTime.DateTime.Month).ToString();
                }
                return _ReportingDateDayOfMonth;
            }
            set
            {
                _ReportingDateDayOfMonth = value;
            }
        }
        public DateTimeOffset TenantDateTime { get; set; }
        public DateTimeOffset ScheduleDate()
        {
            DateTimeOffset dateTimeOffset;
            int dayOfMonth = -1;

            if (string.Equals(ReportingDateDayOfMonth, BureauReporting.Abstractions.Configuration.Common.LastDayOfMonth, StringComparison.OrdinalIgnoreCase))
            {
                dayOfMonth = DateTime.DaysInMonth(TenantDateTime.DateTime.Year, TenantDateTime.DateTime.Month);
            }
            else
            {
                int.TryParse(ReportingDateDayOfMonth, out dayOfMonth);
            }

            dateTimeOffset = new DateTimeOffset(new DateTime(TenantDateTime.DateTime.Year, TenantDateTime.DateTime.Month, dayOfMonth), TenantDateTime.Offset);

            if (dateTimeOffset > TenantDateTime.DateTime.Date)
            {
                return dateTimeOffset;
            }
            else
            {
                return dateTimeOffset.AddMonths(1);
            }
        }
        public string GetCronExpression()
        {
            //Every month on this day(ReportingDateDayOfMonth) at 12am (mid night)
            //return "0 0 0 " + ReportingDateDayOfMonth + " * ?";

            //Test code
            return "0 0/1 * 1/1 * ? *";
            
            //0 0/5 14 * * ? - Every 5 minutes starting at 2 PM and ending at 2:55 PM, every day.
        }
    }
}
