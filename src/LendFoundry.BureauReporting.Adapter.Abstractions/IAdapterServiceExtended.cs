﻿using LendFoundry.BureauReporting.Adapter.Abstractions.Configuration;
using LendFoundry.EventHub.Client;
using LendFoundry.Foundation.Logging;
using LendFoundry.Loans.Client;
using System;

namespace LendFoundry.BureauReporting.Adapter.Abstractions
{
    public interface IAdapterServiceExtended : IAdapterService
    {
        Action<EventInfo> ProcessEvent(EventMapping eventMapping, String reportingDateDayOfMonth, ILoanService loanService, ILogger logger);
    }
}
