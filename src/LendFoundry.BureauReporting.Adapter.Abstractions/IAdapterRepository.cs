﻿using System;
using LendFoundry.Foundation.Persistence;
using System.Collections.Generic;

namespace LendFoundry.BureauReporting.Adapter.Abstractions
{
    public interface IAdapterRepository : IRepository<IAdapterEntry>
    {
        void AddOrUpdate(IAdapterEntry bureauReportData);

        IEnumerable<IAdapterEntry> GetCurrentPeriodData();

        IAdapterEntry GetBureauReportData(string loanReferenceNumber);

        IAdapterEntry GetBureauReportData(string ReferenceNumber, DateTimeOffset CutOffPeriod, bool FetchFromPrevPreiod = false);

        void UpdateSendProcessToBureau(IEnumerable<IAdapterEntry> bureauReportData, DateTimeOffset cutOffPeriod);

        IEnumerable<IAdapterEntry> ScanMissingData(DateTimeOffset CutOffPeriod);

        void AddData(IEnumerable<IAdapterEntry> bureauReportData, DateTimeOffset cutOffPeriod);

        IEnumerable<IAdapterEntry> GetCurrentPeriodData(bool IsSkipped);
    }
}
