﻿using LendFoundry.Loans;
using System;
using System.Collections.Generic;

namespace LendFoundry.BureauReporting.Adapter.Abstractions
{
    public interface ILoanPayment
    {
        int Year { get; }
        int Month { get; }
        double Amount { get; }
        string Code { get; }
        DateTimeOffset Date { get; }
        string Description { get; }
        string Id { get; }
        string LoanReferenceNumber { get; }
        string Notes { get; }
        string PaymentId { get; }
        DateTimeOffset Timestamp { get; }
    }
}
