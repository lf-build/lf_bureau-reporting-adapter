﻿using System;
using System.Collections.Generic;

namespace LendFoundry.BureauReporting.Adapter.Abstractions
{
    public interface IAdapterService
    {
        IEnumerable<IAdapterEntry> GetCurrentPeriodData();

        IAdapterEntry GetBureauReportData(string loanReferenceNumber);

        bool CanSkip(string Code);

        void LogAndRaiseEventHubEvent(string LogInfo, string log, string EventName);
    }    
}
