﻿using System.Collections.Generic;
using LendFoundry.Foundation.Persistence;
using LendFoundry.BureauReporting.Abstractions.Domain;
using System;
using LendFoundry.BureauReporting.Abstractions.Requests.Account;
using LendFoundry.BureauReporting.Abstractions.Requests.Borrower;
using LendFoundry.BureauReporting.Abstractions.Requests.Loan;
using LendFoundry.BureauReporting.Abstractions.Requests.Payments;

namespace LendFoundry.BureauReporting.Adapter.Abstractions
{
    public class AdapterEntry : Aggregate, IAdapterEntry
    {
        public AccountRequestData AccountInfo { get; set; }
        public ICollection<BorrowerRequestData> Borrowers { get; set; }
        public LoanRequestData LoanInfo { get; set; }
        public ICollection<PaymentDetailsRequestData> PaymentHistory { get; set; }

        public bool IsProcessed { get; set; }
        public DateTimeOffset CutOffPeriod { get; set; }
        public bool IsSkipped { get; set; }
    }
}
