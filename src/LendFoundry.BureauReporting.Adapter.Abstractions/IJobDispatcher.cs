﻿namespace LendFoundry.BureauReporting.Adapter.Abstractions
{
    public interface IJobDispatcher
    {
        void Start();
    }
}
