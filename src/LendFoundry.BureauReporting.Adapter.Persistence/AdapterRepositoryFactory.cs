﻿using System;
using LendFoundry.Foundation.Persistence.Mongo;
using LendFoundry.Security.Tokens;
using LendFoundry.Tenant.Client;
using Microsoft.Framework.DependencyInjection;
using LendFoundry.BureauReporting.Adapter.Abstractions;

namespace LendFoundry.BureauReporting.Adapter.Persistence
{
    public class AdapterRepositoryFactory : IAdapterRepositoryFactory
    {
        private IServiceProvider Provider { get; }

        public AdapterRepositoryFactory(IServiceProvider provider)
        {
            Provider = provider;
        }

        public IAdapterRepository Create(ITokenReader reader)
        {
            var tenantServiceFactory = Provider.GetService<ITenantServiceFactory>();
            var tenantService = tenantServiceFactory.Create(reader);
            var mongoConfiguration = Provider.GetService<IMongoConfiguration>();
            return new AdapterRepository(tenantService, mongoConfiguration);
        }
    }
}
