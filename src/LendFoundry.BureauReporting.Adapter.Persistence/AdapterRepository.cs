﻿using LendFoundry.BureauReporting.Abstractions.Domain;
using LendFoundry.BureauReporting.Abstractions.Domain.Account;
using LendFoundry.BureauReporting.Abstractions.Domain.Common;
using LendFoundry.BureauReporting.Abstractions.Domain.Loan;
using LendFoundry.BureauReporting.Abstractions.Domain.Payments;
using LendFoundry.BureauReporting.Abstractions.Requests.Account;
using LendFoundry.BureauReporting.Abstractions.Requests.Common;
using LendFoundry.BureauReporting.Abstractions.Requests.Loan;
using LendFoundry.BureauReporting.Abstractions.Requests.Payments;
using LendFoundry.BureauReporting.Adapter.Abstractions;
using LendFoundry.BureauReporting.Domain.Metro2;
using LendFoundry.Foundation.Persistence.Mongo;
using LendFoundry.Tenant.Client;
using MongoDB.Bson;
using MongoDB.Bson.Serialization;
using MongoDB.Bson.Serialization.Serializers;
using MongoDB.Driver;
using MongoDB.Driver.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace LendFoundry.BureauReporting.Adapter.Persistence
{
    public class AdapterRepository : MongoRepository<IAdapterEntry, AdapterEntry>, IAdapterRepository
    {
        private string TenantId { get; }

        static AdapterRepository()
        {
            BsonClassMap.RegisterClassMap<AdapterEntry>(map =>
            {
                map.AutoMap();
                map.MapProperty(p => p.CutOffPeriod).SetSerializer(new DateTimeOffsetSerializer(BsonType.Document));

                var type = typeof(AdapterEntry);
                map.SetDiscriminator($"{type.FullName}, {type.Assembly.GetName().Name}");
                map.SetIsRootClass(true);

                BsonSerializer.RegisterSerializer(new ImpliedImplementationInterfaceSerializer<IReportData, ReportData>());
            });

            BsonClassMap.RegisterClassMap<AccountRequestData>(map =>
            {
                map.AutoMap();
                map.MapProperty(p => p.DateOfAccountInformation).SetSerializer(new DateTimeOffsetSerializer(BsonType.Document));

                var type = typeof(AccountRequestData);
                map.SetDiscriminator($"{type.FullName}, {type.Assembly.GetName().Name}");
                map.SetIsRootClass(true);
            });

            BsonClassMap.RegisterClassMap<LoanRequestData>(map =>
            {
                map.AutoMap();
                map.MapProperty(p => p.LoanFundedDate).SetSerializer(new DateTimeOffsetSerializer(BsonType.Document));

                var type = typeof(LoanRequestData);
                map.SetDiscriminator($"{type.FullName}, {type.Assembly.GetName().Name}");
                map.SetIsRootClass(true);
            });

            BsonClassMap.RegisterClassMap<PersonRequestData>(map =>
            {
                map.AutoMap();
                map.MapProperty(p => p.DateOfBirth).SetSerializer(new DateTimeOffsetSerializer(BsonType.Document));

                var type = typeof(PersonRequestData);
                map.SetDiscriminator($"{type.FullName}, {type.Assembly.GetName().Name}");
                map.SetIsRootClass(true);
            });

            BsonClassMap.RegisterClassMap<PaymentDetailsRequestData>(map =>
            {
                map.AutoMap();
                map.MapProperty(p => p.DueDate).SetSerializer(new DateTimeOffsetSerializer(BsonType.Document));
                map.MapMember(m => m.PaymentDate).SetSerializer(new NullableSerializer<DateTimeOffset>(new DateTimeOffsetSerializer(BsonType.Document)));

                var type = typeof(PaymentDetailsRequestData);
                map.SetDiscriminator($"{type.FullName}, {type.Assembly.GetName().Name}");
                map.SetIsRootClass(true);
            });

            BsonClassMap.RegisterClassMap<HistoricalData>(map =>
            {
                map.AutoMap();
                var type = typeof(HistoricalData);
                map.MapProperty(p => p.DateOfAccountInformation).SetSerializer(new DateTimeOffsetSerializer(BsonType.Document));

                map.SetDiscriminator($"{type.FullName}, {type.Assembly.GetName().Name}");
                map.SetIsRootClass(true);
            });
        }

        public AdapterRepository(ITenantService tenantService, IMongoConfiguration configuration) :
            base(tenantService, configuration, "queueReportData")
        {
            TenantId = tenantService.Current.Id;
        }

        /// <summary>
        /// Fetching Queue Data
        /// 1. For Current Period
        /// 2. From Previous Peeriod 
        /// </summary>
        /// <param name="ReferenceNumber">Loan Number</param>
        /// <param name="CutOffPeriod">CutOffDate</param>
        /// <param name="FetchPrevPreiod">Flag</param>
        /// <returns></returns>
        public IAdapterEntry GetBureauReportData(string LoanReferenceNumber, DateTimeOffset CutOffPeriod, bool FetchPrevPreiod = false)
        {
            if (FetchPrevPreiod)
            {
                return Query
                .Where
                (
                    r => r.TenantId == TenantId &&
                    r.LoanInfo.LoanNumber == LoanReferenceNumber &&
                    (r.CutOffPeriod > CutOffPeriod.AddMonths(-2)) &&
                    r.IsProcessed == true
                )
                .SingleOrDefault();
            }
            else
            {
                return GetBureauReportData(LoanReferenceNumber);
            }
        }

        /// <summary>
        /// Adding | Updating Queue Data
        /// </summary>
        /// <param name="bureauReportData">Queue data</param>
        public void AddOrUpdate(IAdapterEntry bureauReportData)
        {
            if (String.IsNullOrWhiteSpace(bureauReportData.Id))
            {
                bureauReportData.Id = ObjectId.GenerateNewId().ToString();
                Collection.InsertOne(bureauReportData);
            }
            else
            {
                Expression<Func<IAdapterEntry, bool>> bureauReportDataAlreadyCreated = r =>
                    r.Id == bureauReportData.Id;

                Collection.ReplaceOne(bureauReportDataAlreadyCreated, bureauReportData);
            }
        }

        /// <summary>
        /// Get QueueData For Current Period
        /// </summary>
        /// <returns>IEnumerable<IAdapterEntry></returns>
        public IEnumerable<IAdapterEntry> GetCurrentPeriodData()
        {
            return Query
                .Where
                (
                    r => r.TenantId == TenantId &&
                    r.IsProcessed == false
                );
        }

        /// <summary>
        /// 1. Scan MissingData Per Current Period
        /// 2. Add Into Queue
        /// </summary>
        /// <param name="CutOffPeriod">CutOffPeriod</param>
        public IEnumerable<IAdapterEntry> ScanMissingData(DateTimeOffset cutOffPeriod)
        {
            string[] LoanNumbers = GetCurrentPeriodData().Select(d => d.LoanInfo.LoanNumber).ToArray();

            return Query
                .Where
                (
                    r => r.TenantId == TenantId &&
                    (r.CutOffPeriod > cutOffPeriod.AddMonths(-2).AddDays(-1)) &&
                    r.IsProcessed == true &&
                    !LoanNumbers.Contains(r.LoanInfo.LoanNumber)
                ).ToList();
        }

        /// <summary>
        /// Get QueueData Per LoanReferenceNumber
        /// </summary>
        /// <param name="loanReferenceNumber">loanReferenceNumber</param>
        /// <returns>IAdapterEntry</returns>
        public IAdapterEntry GetBureauReportData(string loanReferenceNumber)
        {
            return Query
                .Where
                (
                    r => r.TenantId == TenantId &&
                    r.LoanInfo.LoanNumber == loanReferenceNumber &&
                    r.IsProcessed == false
                )
                .SingleOrDefault();
        }

        /// <summary>
        /// Update QueueData After Send To BureauReporting API
        /// </summary>
        /// <param name="queueData">ICollection<IAdapterEntry></param>
        public void UpdateSendProcessToBureau(IEnumerable<IAdapterEntry> queueData, DateTimeOffset cutOffPeriod)
        {
            var bulk = new List<WriteModel<IAdapterEntry>>();

            Parallel.ForEach(queueData, record =>
            {
                record.IsProcessed = true;
                record.CutOffPeriod = cutOffPeriod;

                var filter = new ExpressionFilterDefinition<IAdapterEntry>(p =>
                      p.TenantId == TenantId &&
                      p.TenantId == record.TenantId &&
                      p.Id == record.Id);

                var model = new ReplaceOneModel<IAdapterEntry>(filter, record);

                bulk.Add(model);
            });

            Collection.BulkWrite(bulk);
        }

        /// <summary>
        /// Add QueueData Into Current Period
        /// </summary>
        /// <param name="queueData"></param>
        public void AddData(IEnumerable<IAdapterEntry> queueData, DateTimeOffset cutOffPeriod)
        {
            if (queueData == null || (queueData != null && queueData.Count() == 0))
                throw new ArgumentNullException(nameof(queueData));

            Parallel.ForEach(queueData, r =>
            {
                r.IsProcessed = false;
                r.TenantId = TenantId;
                r.Id = ObjectId.GenerateNewId().ToString();
                r.CutOffPeriod = cutOffPeriod;
            });

            Collection.InsertMany(queueData);
        }

        /// <summary>
        /// Get QueueData NonSkipped Only From CurrentPeriod
        /// </summary>
        /// <param name="IsSkipped">bool</param>
        /// <returns>IEnumerable<IAdapterEntry></returns>
        public IEnumerable<IAdapterEntry> GetCurrentPeriodData(bool IsSkipped)
        {
            return Query
                .Where
                (
                    r => r.TenantId == TenantId &&
                    r.IsProcessed == false &&
                    r.IsSkipped == IsSkipped
                );
        }
    }
}
