﻿using Microsoft.AspNet.Builder;
using Microsoft.Framework.DependencyInjection;
using LendFoundry.BureauReporting.Adapter.Abstractions;

namespace LendFoundry.BureauReporting.Adapter
{
    public static class AdapterListenerExtensions
    {
        public static void UseAdapterListener(this IApplicationBuilder app)
        {
            app.ApplicationServices.GetRequiredService<IAdapterListener>().Start();
        }
    }
}
