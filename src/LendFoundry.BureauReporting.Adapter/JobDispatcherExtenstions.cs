﻿using LendFoundry.BureauReporting.Adapter.Abstractions;
using Microsoft.AspNet.Builder;
using Microsoft.Framework.DependencyInjection;

namespace LendFoundry.BureauReporting.Adapter
{
    public static class JobDispatcherExtenstions
    {
        public static void UseJobDispacher(this IApplicationBuilder app)
        {
            app.ApplicationServices.GetRequiredService<IJobDispatcher>().Start();
        }
    }
}
