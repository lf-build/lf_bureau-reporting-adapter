﻿using LendFoundry.BureauReporting.Adapter.Abstractions;
using LendFoundry.BureauReporting.Adapter.Abstractions.Configuration;
using LendFoundry.Configuration.Client;
using LendFoundry.EventHub.Client;
using LendFoundry.Foundation.Logging;
using LendFoundry.Security.Tokens;
using LendFoundry.Tenant.Client;
using Microsoft.Framework.DependencyInjection;
using System;

namespace LendFoundry.BureauReporting.Adapter
{
    // This project can output the Class library as a NuGet Package.
    // To enable this option, right-click on the project and select the Properties menu item. In the Build tab select "Produce outputs on build".
    public class AdapterServiceFactory : IAdapterServiceFactory
    {
        public AdapterServiceFactory(IServiceProvider provider)
        {
            Provider = provider;
        }

        private IServiceProvider Provider { get; }

        public IAdapterServiceExtended Create(StaticTokenReader reader, ILogger logger)
        {
            var repositoryFactory = Provider.GetService<IAdapterRepositoryFactory>();
            var repository = repositoryFactory.Create(reader);

            var eventHubFactory = Provider.GetService<IEventHubClientFactory>();
            var eventhub = eventHubFactory.Create(reader);

            var configurationService = Provider.GetService<IConfigurationService<AdapterConfiguration>>();

            var tenantInfo = Provider.GetService<Tenant.Client.ITenantServiceFactory>().Create(reader).Current;

            return new AdapterService(repository, logger, configurationService, tenantInfo, eventhub);
        }

        public IAdapterServiceExtended Create(StaticTokenReader reader, ILogger logger, TenantInfo tenantInfo)
        {
            var repositoryFactory = Provider.GetService<IAdapterRepositoryFactory>();
            var repository = repositoryFactory.Create(reader);

            var eventHubFactory = Provider.GetService<IEventHubClientFactory>();
            var eventhub = eventHubFactory.Create(reader);

            var configurationService = Provider.GetService<IConfigurationService<AdapterConfiguration>>();

            return new AdapterService(repository, logger, configurationService, tenantInfo, eventhub);
        }
    }
}
