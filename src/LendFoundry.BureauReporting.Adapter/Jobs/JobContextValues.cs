﻿using LendFoundry.Configuration.Client;
using LendFoundry.Foundation.Date;
using LendFoundry.Foundation.Logging;
using LendFoundry.Loans.Client;
using LendFoundry.Security.Tokens;
using Microsoft.Framework.DependencyInjection;
using Quartz;
using System;
using LendFoundry.EventHub.Client;
using LendFoundry.BureauReporting.Adapter.Abstractions;
using LendFoundry.BureauReporting.Adapter.Abstractions.Configuration;
using LendFoundry.BureauReporting.Client;
using LendFoundry.BureauReporting.Abstractions.Services;
using LendFoundry.Notifications.Client;
using LendFoundry.Foundation.Services;

namespace LendFoundry.BureauReporting.Adapter.Jobs
{
    public class JobContextValues
    {
        #region Protected Properties
        protected ConfigurationKeys ConfigurationKeys { get; set; }

        protected ILogger Logger { get; private set; }

        protected IAdapterRepository Repository { get; private set; }

        protected IReportDataService BureauReportingService { get; private set; }

        protected ITenantTime TenantTime { get; private set; }

        protected IScheduleExecutorFactory ExecutorFactory { get; private set; }

        protected IEventHubClient EventHub { get; private set; }

        protected INotificationService NotificationService { get; private set; }
        protected IAdapterService AdapterService { get; private set; }
        protected ILoanService LoanService { get; private set; }
        #endregion

        public void ResolveContextValues(IJobExecutionContext context)
        {
            var provider = context.MergedJobDataMap.Get("provider") as IServiceProvider;
            var reader = context.MergedJobDataMap.Get("tenantReader") as StaticTokenReader;

            Logger = provider.GetService<ILoggerFactory>().CreateLogger();
            Repository = provider.GetService<IAdapterRepositoryFactory>().Create(reader);

            BureauReportingService = provider.GetService<IBureauReportingFactory>().Create(reader);

            var configurationFactory = provider.GetService<IConfigurationServiceFactory>();
            TenantTime = provider.GetService<ITenantTimeFactory>().Create(configurationFactory, reader);

            ExecutorFactory = provider.GetService<IScheduleExecutorFactory>();

            ConfigurationKeys = configurationFactory.Create<ConfigurationKeys>(Settings.ServiceName, reader).Get();
            ConfigurationKeys.TenantDateTime = TenantTime.Now;
            
            EventHub = provider.GetService<IEventHubClientFactory>().Create(reader);

            NotificationService = provider.GetService<INotificationServiceFactory>().Create(reader);

            AdapterService = provider.GetService<IAdapterServiceFactory>().Create(reader, Logger);

            LoanService = provider.GetService<ILoanServiceFactory>().Create(reader);
        }

        protected void Run(Action expression)
        {
            try
            {
                expression.Invoke();
            }
            catch (Exception ex)
            {
                if (ex.InnerException != null && ex.InnerException is ClientException)
                {
                    Logger.Error(ex.InnerException.Message);
                }
                else
                {
                    Logger.Error("Unhandled exception while executing Job:", ex);
                }
            }
        }
    }
}
