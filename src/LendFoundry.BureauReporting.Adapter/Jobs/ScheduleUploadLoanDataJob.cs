﻿using LendFoundry.Foundation.Date;
using Quartz;
using System.Linq;

namespace LendFoundry.BureauReporting.Adapter.Jobs
{
    public class ScheduleUploadLoanDataJob : JobContextValues, IJob
    {
        public void Execute(IJobExecutionContext context)
        {
            Run(() =>
            {
                ResolveContextValues(context);
                Logger.Info($"Task {nameof(ScheduleUploadLoanDataJob)} started");
                Logger.Info("Checking for scheduled entries...");

                var todaySchedules = ConfigurationKeys.ScheduleDate();

                Logger.Info($"Task schedueld at {todaySchedules}.");

                var scheduleExecutor = ExecutorFactory.Create(BureauReportingService, Logger, Repository, EventHub, 
                    NotificationService, AdapterService, LoanService);
                scheduleExecutor.RunIt(todaySchedules);
                Logger.Info($"Task {nameof(ScheduleUploadLoanDataJob)} executed");
            });
        }
    }
}
