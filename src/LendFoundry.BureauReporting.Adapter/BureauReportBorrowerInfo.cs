﻿using LendFoundry.BureauReporting.Abstractions.Domain.Borrower;
using LendFoundry.BureauReporting.Abstractions.Domain.Common;
using LendFoundry.BureauReporting.Abstractions.Domain.Lookups;
using LendFoundry.BureauReporting.Abstractions.Requests.Borrower;
using LendFoundry.BureauReporting.Abstractions.Requests.Common;
using LendFoundry.Loans;
using System.Collections.Generic;
using System.Linq;

namespace LendFoundry.BureauReporting.Adapter
{
    public class BureauReportBorrowerInfo
    {
        #region Private Properties
        private ILoanDetails LoanDetails { get; set; }

        private List<BorrowerRequestData> Borrowers { get; set; }
        #endregion

        #region Private Methods
        #region BorrowerInfo
        /// <summary>
        /// Map Borrower (Loan data) to BorrowerInfo (Bureau report data)
        /// </summary>
        /// <param name="borrower">IBorrower</param>
        /// <returns>BorrowerInfo</returns>
        private BorrowerRequestData BorrowerInfo(IBorrower borrower)
        {
            return new BorrowerRequestData
            {
                //TODO: As of now, there is only Primary. But need to enhance this feature later in Loan
                Role = (borrower.IsPrimary ? Lookups.PrimaryBorrower : ""),
                Status = BorrowerStatus(LoanDetails.LoanInfo.Loan.Status.Code),
                PersonalData = PersonalData(borrower),
                Address = Address(borrower),
                Traceability = SkipTrace(),
                Bankruptcy = (LoanDetails.LoanInfo.Loan.Status.Code == "100.09" ? Bankruptcy() : null),
                //TODO: As of now, there is no employment ingo. But need to enhance this feature later in Loan
                Employment = null
            };
        }
        #endregion

        #region Address
        private AddressRequestData Address(IBorrower Borrower)
        {
            Loans.Address Address = null;
            Loans.Phone Telephone = null;

            if (Borrower.Addresses.Count() == 1)
            {
                Address = Borrower.Addresses.ToList()[0];
            }
            else
            {
                Address = Borrower.Addresses.Where(add => add.IsPrimary == true).SingleOrDefault();
            }

            if (Borrower.Phones.Count() == 1)
            {
                Telephone = Borrower.Phones.ToList()[0];
            }
            else
            {
                Telephone = Borrower.Phones.Where(ph => ph.IsPrimary == true).SingleOrDefault();
            }

            return new AddressRequestData
            {
                City = Address.City,
                CountryCode = "US",
                Line1 = Address.Line1,
                Line2 = Address.Line2,
                ResidentialStatus = ResidentialStatus(),
                State = Address.State,
                ZipCode = Address.ZipCode,
                Telephone = Telephone?.Number
            };
        }
        #endregion

        #region ResidentialStatus
        private string ResidentialStatus()
        {
            var HomeOwnership = LoanDetails.LoanInfo.Loan.HomeOwnership.ToUpper();

            if (HomeOwnership.Contains("RENT"))
            {
                return Lookups.ResidentialStatusRent;
            }
            else if (HomeOwnership.Contains("OWN") || HomeOwnership.Contains("OWNER") || HomeOwnership.Contains("MORTGAGE"))
            {
                return Lookups.ResidentialStatusOwn;
            }
            else
            {
                return string.Empty;
            }
        }
        #endregion

        #region PersonalData
        private PersonRequestData PersonalData(IBorrower Borrower)
        {
            return new PersonRequestData
            {
                DateOfBirth = Borrower.DateOfBirth,
                FirstName = Borrower.FirstName,
                LastName = Borrower.LastName,
                MiddleName = Borrower.MiddleInitial,
                SocialSecurityNumber = Borrower.SSN
            };
        }
        #endregion        

        #region Bankruptcy
        private BankruptcyStatusRequestData Bankruptcy()
        {
            //TODO: BankruptcyTypes
            //TODO: BankruptcyStatuses
            //TODO: Need to map Type and Status
            return new BankruptcyStatusRequestData
            {
                Type = "",
                Status = "",
                PetitionDate = null,
                DischargeDate = null,
                WithdrawnDate = null
            };
        }
        #endregion

        #region SkipTrace
        private TraceabilityRequestData SkipTrace()
        {
            bool canTrace = LoanDetails.LoanInfo.Loan.Status.Code == "100.06" ? false : true;
            return new TraceabilityRequestData
            {
                InSkiptrace = !canTrace,
                OverEmail = canTrace,
                OverPhone = canTrace,
                OverPost = canTrace
            };
        }
        #endregion

        #region BorrowerStatus
        public string BorrowerStatus(string Code)
        {
            switch (Code)
            {
                case "100.01":
                case "100.02":
                case "100.03":
                case "100.04":
                case "100.05":
                case "100.06":
                case "100.07":
                case "100.09":
                    return Lookups.BorrowerStatusActive;
                case "300.02":
                    return Lookups.BorrowerStatusDeceased;
                case "200.06":
                    return Lookups.BorrowerStatusCeaseAndDesist;
                case "200.05":
                    return Lookups.BorrowerStatusIncarcerated;
                case "100.08":
                    return Lookups.BorrowerStatusCivilService;
                default:
                    return Lookups.BorrowerStatusActive;
            }
        }
        #endregion
        #endregion

        #region Public Methods
        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="LoanDetails">ILoanDetails</param>
        public BureauReportBorrowerInfo(ILoanDetails LoanDetails)
        {
            this.LoanDetails = LoanDetails;
        }

        /// <summary>
        /// Map to BorrowerInfo data
        /// </summary>
        /// <returns></returns>
        public List<BorrowerRequestData> MapBorrowerInfo()
        {
            if (LoanDetails != null)
            {
                Borrowers = new List<BorrowerRequestData>();

                foreach (IBorrower borrower in LoanDetails.LoanInfo.Loan.Borrowers)
                {
                    Borrowers.Add(BorrowerInfo(borrower));
                }
            }

            return Borrowers;
        }
        #endregion
    }
}
