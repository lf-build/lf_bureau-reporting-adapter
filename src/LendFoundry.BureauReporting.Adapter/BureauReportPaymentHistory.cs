﻿using LendFoundry.BureauReporting.Abstractions.Domain.Payments;
using LendFoundry.BureauReporting.Abstractions.Requests.Payments;
using LendFoundry.BureauReporting.Adapter.Abstractions;
using LendFoundry.Loans;
using System;
using System.Collections.Generic;
using System.Linq;

namespace LendFoundry.BureauReporting.Adapter
{
    public class BureauReportPaymentHistory
    {
        #region Private Properties
        private ILoanDetails LoanDetails { get; set; }
        private ICollection<PaymentDetailsRequestData> PaymentHistory { get; set; }
        private DateTimeOffset CutOffPeriod { get; set; }
        #endregion

        #region Private Methods
        /// <summary>
        /// Map to ScheduledAmount
        /// </summary>
        /// <returns>Amount</returns>
        private AmountRequestData ScheduledAmount()
        {
            return new AmountRequestData
            {
                Charges = 0,
                Fees = 0,
                Interest = 0,
                Principal = (LoanDetails.LoanInfo.Summary.NextInstallmentAmount == null ? 0 : (double)LoanDetails.LoanInfo.Summary.NextInstallmentAmount)
            };
        }

        /// <summary>
        /// Map to AmountPaid
        /// 1. Adding Loan paymwnt amount with prev. principal paid amount
        /// </summary>
        /// <returns>Amount</returns>
        private AmountRequestData AmountPaid(ILoanPayment LoanPayment, double PrevPrincipal)
        {
            return new AmountRequestData
            {
                Charges = 0,
                Fees = 0,
                Interest = 0,
                Principal = LoanPayment.Amount + PrevPrincipal
            };
        }

        /// <summary>
        /// Map to AmountPastDue
        /// </summary>
        /// <returns>Amount</returns>
        private AmountRequestData AmountPastDue()
        {
            return new AmountRequestData
            {
                Charges = 0,
                Fees = 0,
                Interest = 0,
                Principal = LoanDetails.LoanInfo.Summary.CurrentDue
            };
        }

        /// <summary>
        /// Map to PaymentDetails
        /// </summary>
        /// <param name="Transaction">ITransaction</param>
        /// <returns>PaymentDetails</returns>
        private PaymentDetailsRequestData PaymentDetails(ILoanPayment LoanPayment, double PrevPrincipal)
        {
            return new PaymentDetailsRequestData
            {
                AmountPaid = AmountPaid(LoanPayment, PrevPrincipal),
                AmountPastDue = AmountPastDue(),
                DaysPastDue = LoanDetails.LoanInfo.Summary.DaysPastDue,
                DueDate = LoanPayment.Date,
                PaymentDate = LoanPayment.Timestamp,
                ScheduledAmount = ScheduledAmount()
            };
        }

        /// <summary>
        /// Handling following actions
        /// 1. Fetching only "Principal and Interest"
        /// 2. GroupBy "Year" and "Month"
        /// 3. Creating new object model {Year, Month, List<ITransaction>}
        /// 4. Converting new object model into LoanPayments
        /// 5. Now, one record per Month
        /// </summary>
        /// <returns>List<ILoanPayment></returns>
        private List<ILoanPayment> MapToLoanPayments()
        {
            List<ILoanPayment> LoanPayments = null;

            if (LoanDetails.Transactions != null && LoanDetails.Transactions.Count() > 0)
            {
                IEnumerable<LoanPaymentData> Payments = GetPayments();

                #region Merging transactions record (if more per month) into one transaction record
                //1. Could be more than one transaction per month
                //2. Merging those transactions into one transaction
                if (Payments != null && Payments.Count() > 0)
                {
                    LoanPayments = new List<ILoanPayment>();
                    foreach (var payment in Payments)
                    {
                        var loanPayment = new LoanPayment
                        {
                            Month = payment.Month,
                            Year = payment.Year,
                            Amount = 0
                        };

                        foreach (var p in payment.Transactions)
                        {
                            loanPayment.Id = p.Id;
                            loanPayment.Amount += p.Amount;
                            loanPayment.Code = p.Code;
                            loanPayment.Date = p.Date;
                            loanPayment.Description = p.Description;
                            loanPayment.LoanReferenceNumber = p.LoanReferenceNumber;
                            loanPayment.Notes = p.Notes;
                            loanPayment.PaymentId = p.PaymentId;
                            loanPayment.Timestamp = p.Timestamp;
                        }

                        LoanPayments.Add(loanPayment);
                    }
                }
                #endregion
            }

            return LoanPayments;
        }

        #region Fetching transaction records group by Year and Month
        /// <summary>
        /// Converting into group by Month & Year
        /// </summary>
        /// <param name="IsRecordExists"></param>
        /// <returns></returns>
        private IEnumerable<LoanPaymentData> GetPayments()
        {
            IEnumerable<LoanPaymentData> Payments = new List<LoanPaymentData>();

            if (LoanDetails != null && LoanDetails.Transactions != null && LoanDetails.Transactions.Count() > 0)
            {
                string[] failedPaymentId = LoanDetails.Transactions.Where(t => t.Code == "125").Select(t => t.PaymentId)?.ToArray();

                if (failedPaymentId != null && failedPaymentId.Length > 0)
                {
                    Payments = LoanDetails.Transactions
                                .Where(t => !failedPaymentId.Contains(t.PaymentId) && t.Code == "124")
                                .GroupBy(x => new { x.Timestamp.Year, x.Timestamp.Month }, (key, group) => new LoanPaymentData
                                {
                                    Year = key.Year,
                                    Month = key.Month,
                                    Transactions = group.ToList()
                                });
                }
                else
                {
                    Payments = LoanDetails.Transactions
                                .Where(t => t.Code == "124")
                                .GroupBy(x => new { x.Timestamp.Year, x.Timestamp.Month }, (key, group) => new LoanPaymentData
                                {
                                    Year = key.Year,
                                    Month = key.Month,
                                    Transactions = group.ToList()
                                });
                }                
            }
            return Payments;
        }
        #endregion

        #endregion

        #region Public Methods
        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="LoanDetails">ILoanDetails</param>
        public BureauReportPaymentHistory(ILoanDetails LoanDetails, DateTimeOffset CutOffPeriod)
        {
            this.CutOffPeriod = CutOffPeriod;
            this.LoanDetails = LoanDetails;
        }

        /// <summary>
        /// Map to PaymentHistory data
        /// </summary>
        /// <returns>ICollection<PaymentDetails></returns>
        public ICollection<PaymentDetailsRequestData> MapPaymentHistory()
        {
            if (LoanDetails != null && LoanDetails.Transactions != null)
            {
                PaymentHistory = new List<PaymentDetailsRequestData>();
                var LoanPayments = MapToLoanPayments();

                if (LoanPayments != null && LoanPayments.Count > 0)
                {
                    PaymentDetailsRequestData Payment = null;
                    double PrevPrincipal = 0;
                    foreach (ILoanPayment LoanPayment in LoanPayments)
                    {
                        Payment = PaymentDetails(LoanPayment, PrevPrincipal);
                        PrevPrincipal = Payment.AmountPaid.Principal;
                        PaymentHistory.Add(Payment);
                    }
                }
            }

            return PaymentHistory;
        }
        #endregion
    }

    #region LoanPaymentData
    internal class LoanPaymentData
    {
        public int Year { get; set; }
        public int Month { get; set; }
        public List<ITransaction> Transactions;
    }
    #endregion
}
