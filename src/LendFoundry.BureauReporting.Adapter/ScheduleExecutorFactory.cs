﻿using LendFoundry.BureauReporting.Abstractions.Services;
using LendFoundry.BureauReporting.Adapter.Abstractions;
using LendFoundry.EventHub.Client;
using LendFoundry.Foundation.Logging;
using LendFoundry.Loans.Client;
using LendFoundry.Notifications.Client;

namespace LendFoundry.BureauReporting.Adapter
{
    public class ScheduleExecutorFactory : IScheduleExecutorFactory
    {
        public IScheduleExecutor Create
        (
            IReportDataService bureauReportingService,
            ILogger logger,
            IAdapterRepository repository,
            IEventHubClient eventHub,
            INotificationService notificationService,
            IAdapterService adapterService,
            ILoanService loanService
        )
        => new ScheduleExecutor(bureauReportingService, logger, repository, eventHub, notificationService, adapterService, loanService);
    }
}
