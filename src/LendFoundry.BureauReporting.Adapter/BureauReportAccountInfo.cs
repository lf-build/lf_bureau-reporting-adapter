﻿using LendFoundry.BureauReporting.Abstractions.Domain.Account;
using LendFoundry.BureauReporting.Abstractions.Domain.Lookups;
using LendFoundry.BureauReporting.Abstractions.Domain.Payments;
using LendFoundry.BureauReporting.Abstractions.Requests.Account;
using LendFoundry.BureauReporting.Abstractions.Requests.Payments;
using LendFoundry.Loans;
using System;
using System.Linq;

namespace LendFoundry.BureauReporting.Adapter
{
    public class BureauReportAccountInfo
    {
        #region Private Properties
        private ILoanDetails LoanDetails { get; set; }

        private AccountRequestData AccountInfo { get; set; }
        #endregion

        #region Private Methods
        /// <summary>
        /// Handling Loan Status
        /// </summary>
        private void HandleLoanStatus()
        {
            switch (LoanDetails.LoanInfo.Loan.Status.Code)
            {
                #region 100 - Active [100.01 - 100.09]
                case "100.01": //OnBoarding
                case "100.02": //OnBoarded
                case "100.03": //InService
                case "100.07": //Forbearance
                case "100.08": //CivilReliefAct
                    Current();
                    break;
                case "100.04": //Delinquent
                case "100.06": //SkipTrace
                case "100.09": //Bankruptcy
                    Delinquent(LoanDetails.LoanInfo.Loan.Status.Code);
                    break;
                case "100.05": //Collections
                    Collections();
                    break;
                #endregion

                #region 200 - Situation [200.01 - 200.06]
                case "200.01": //Legal
                case "200.02": //Research
                case "200.03": //Fraud
                case "200.04": //Other Issues
                case "200.05": //Incarcerated
                case "200.06": //Cease & Desist
                    //Could be current or delinquent (As of now, skipping)
                    break;
                #endregion

                #region 300 - Closed  [300.01 - 300.05]
                case "300.01": //Charged Off
                case "300.02": //Deceased
                case "300.03": //Bankruptcy - PaidOff
                case "300.04": //Bankruptcy - Discharged
                    Delinquent(LoanDetails.LoanInfo.Loan.Status.Code);
                    break;
                case "300.05": //Closed - Paid in Full
                    Current();
                    break;
                    #endregion
            }
        }

        #region Current
        private void Current()
        {
            AccountInfo.PaymentStatus = Lookups.PaymentStatusCurrent;
            AccountInfo.TotalAmountDue = TotalAmountDue();

            //What are the specific meaning of these following label
            //Active - Forbearance
            //Active - CivilRelief
        }
        #endregion

        #region Delinquent
        private void Delinquent(string Code)
        {
            AccountInfo.PaymentStatus = Lookups.PaymentStatusDelinquent;
            AccountInfo.TotalAmountDue = TotalAmountDue();
            AccountInfo.Delinquency = new DelinquencyStatusRequestData
            {
                DelinquentAmount = (int)LoanDetails.LoanInfo.Summary.RemainingBalance,
                LastDelinquentDate = LoanDetails.LoanInfo.Summary.LastPaymentDate.Value,
                NumberOfDaysDelinquent = LoanDetails.LoanInfo.Summary.DaysPastDue
            };

            switch (Code)
            {
                case "100.06": //SkipTrace
                    //Traceability is handling in Borrowers map section
                    break;
                case "100.09": //Bankruptcy
                    //Bankruptcy is handling in Borrowers map section
                    break;
                case "300.01": //Charged Off
                    ChargeOff("");
                    break;
                case "300.02": //Deceased
                    ChargeOff("");
                    break;
                case "300.03": //Bankruptcy - PaidOff
                    //Bankruptcy is handling in Borrowers map section
                    break;
                case "300.04": //Bankruptcy - Discharged
                    //Bankruptcy is handling in Borrowers map section
                    break;
            }
        }
        #endregion

        #region Collections
        private void Collections()
        {
            AccountInfo.PaymentStatus = Lookups.PaymentStatusCollection;
            //Need to add following info
            //1. Settlement
            //2. Closure
        }
        #endregion

        #region Dispute
        private DisputeStatus Dispute(string Status)
        {
            return new DisputeStatus
            {
                BorrowerAgrees = true,
                Status = Status,
                TotalAmountDue = new Amount
                {
                    Charges = 0,
                    Fees = 0,
                    Interest = 0,
                    Principal = 0
                }
            };
        }
        #endregion

        #region Charge Off
        private ChargeOffStatus ChargeOff(string Status)
        {
            return new ChargeOffStatus
            {
                Status = Status,
                ChargeOffAmount = 0,
                CompletionDate = DateTime.Now,
                InitiatedDate = DateTime.Now,
                RecommendedDate = DateTime.Now,
                TotalAmountDue = new Amount
                {
                    Charges = 0,
                    Fees = 0,
                    Interest = 0,
                    Principal = 0
                }
            };
        }
        #endregion

        #region Closure
        private ClosureStatus Closure(string Status)
        {
            return new ClosureStatus
            {
                Status = Status,
                ClosureDate = DateTime.Now,
                ReleaseDate = DateTime.Now,
                TotalAmountDue = new Amount
                {
                    Charges = 0,
                    Fees = 0,
                    Interest = 0,
                    Principal = 0
                }
            };
        }
        #endregion

        #region Settlement
        private SettlementStatus Settlement(string Status)
        {
            return new SettlementStatus
            {
                Status = Status,
                LastPaymentAmount = 0,
                LastPaymentDate = DateTime.Now,
                TotalAmountDue = new Amount
                {
                    Charges = 0,
                    Fees = 0,
                    Interest = 0,
                    Principal = 0
                }
            };
        }
        #endregion

        #region Total Amount Due
        private AmountRequestData TotalAmountDue()
        {
            return new AmountRequestData
            {
                Charges = 0,
                Fees = 0,
                Interest = 0,
                Principal = LoanDetails.LoanInfo.Summary.RemainingBalance
            };
        }
        #endregion
        #endregion

        #region Public Methods
        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="LoanDetails">ILoanDetails</param>
        public BureauReportAccountInfo(ILoanDetails LoanDetails)
        {
            this.LoanDetails = LoanDetails;
        }

        /// <summary>
        /// Map to AccountInfo data
        /// </summary>
        /// <returns></returns>
        public AccountRequestData MapAccountInfo()
        {
            if (LoanDetails != null)
            {
                AccountInfo = new AccountRequestData();
                //TODO: We may need to capture this field in Loan system
                AccountInfo.InterestType = Lookups.FixedInterest;                
                HandleLoanStatus();
            }
            return AccountInfo;
        }
        #endregion
    }
}