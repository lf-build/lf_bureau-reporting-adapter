﻿using LendFoundry.BureauReporting.Adapter.Abstractions;
using LendFoundry.BureauReporting.Adapter.Abstractions.Configuration;
using LendFoundry.Configuration.Client;
using LendFoundry.EventHub.Client;
using LendFoundry.Foundation.Logging;
using LendFoundry.Loans.Client;
using LendFoundry.Security.Tokens;
using LendFoundry.Tenant.Client;

namespace LendFoundry.BureauReporting.Adapter
{
    public class AdapterListener: IAdapterListener
    {
        #region Private Members
        private IAdapterServiceFactory ServiceFactory { get; }

        private IConfigurationServiceFactory<AdapterConfiguration> ConfigurationFactory { get; }

        private IEventHubClientFactory EventHubFactory { get; }

        private ITokenHandler TokenHandler { get; }

        private ITenantServiceFactory TenantServiceFactory { get; }

        private ILoanServiceFactory LoanServiceFactory { get; }

        private ILoggerFactory LoggerFactory { get; }
        #endregion

        #region Public Members
        public AdapterListener (IAdapterServiceFactory serviceFactory,
            IConfigurationServiceFactory<AdapterConfiguration> configurationFactory,
            IEventHubClientFactory eventHubFactory,
            ITokenHandler tokenHandler,
            ITenantServiceFactory tenantServiceFactory,
            ILoanServiceFactory loanServiceFactory,
            ILoggerFactory loggerFactory)
        {
            ServiceFactory = serviceFactory;
            ConfigurationFactory = configurationFactory;
            EventHubFactory = eventHubFactory;
            TokenHandler = tokenHandler;
            TenantServiceFactory = tenantServiceFactory;
            LoanServiceFactory = loanServiceFactory;
            LoggerFactory = loggerFactory;
        }        

        public void Start()
        {
            var logger = LoggerFactory.Create(NullLogContext.Instance);
            logger.Info($"Starting Loan Adapter listener...");

            try
            {
                var emptyReader = new StaticTokenReader(string.Empty);
                var eventhub = EventHubFactory.Create(emptyReader);

                var tenantService = TenantServiceFactory.Create(emptyReader);
                var tenants = tenantService.GetActiveTenants();
                logger.Info($"#{tenants.Count} active tenant(s) found to be processed");

                // Get all active tenants in place and set an eventhub subscription for each one.
                tenants.ForEach(tenant =>
                {
                    logger.Info($"Processing tenant #{tenant.Id}");
                    var token = TokenHandler.Issue(tenant.Id, Settings.ServiceName);
                    var reader = new StaticTokenReader(token.Value);
                    var loanService = LoanServiceFactory.Create(reader);

                    var configurationService = ConfigurationFactory.Create(reader);
                    var configuration = configurationService.Get();
                    if (configuration != null)
                    {
                        var service = ServiceFactory.Create(reader, logger, tenant);
                        foreach (var eventConfiguration in configuration.Events)
                        {                           
                            eventhub.On(eventConfiguration.Name, service.ProcessEvent(eventConfiguration, configuration.ReportingDateDayOfMonth, loanService, logger));
                            logger.Info($"Attached eventhub subscription for  #{eventConfiguration.Name}");
                        }
                    }
                    else
                    {
                        logger.Info("The configuration can't be found");
                    }
                });
                eventhub.StartAsync();
                logger.Info($"Loan Adapter listener started");
            }
            catch (WebSocketSharp.WebSocketException ex)
            {
                logger.Error("Error while listening eventhub to process loan Adapter", ex);
                logger.Info("Trying to reset service..");
                Start();
            }
        }
        #endregion
    }
}
