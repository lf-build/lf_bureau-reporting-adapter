﻿using LendFoundry.BureauReporting.Abstractions.Domain.Loan;
using LendFoundry.BureauReporting.Abstractions.Requests.Loan;
using LendFoundry.Loans;

namespace LendFoundry.BureauReporting.Adapter
{
    public class BureauReportLoanInfo
    {
        #region Private Properties
        private ILoanDetails LoanDetails { get; set; }
        private LoanRequestData LoanInfo { get; set; }
        #endregion

        #region Public Methods
        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="LoanDetails">ILoanDetails</param>
        public BureauReportLoanInfo(ILoanDetails LoanDetails)
        {
            this.LoanDetails = LoanDetails;
        }

        /// <summary>
        /// Map to LoanInfo data
        /// </summary>
        /// <returns>LoanInfo</returns>
        public LoanRequestData MapLoanInfo()
        {
            if (LoanDetails != null)
            {
                LoanInfo = new LoanRequestData();

                LoanInfo.FundedLoanAmount = (int)LoanDetails.LoanInfo.Loan.Terms.LoanAmount;
                LoanInfo.LoanFundedDate = LoanDetails.LoanInfo.Loan.Terms.FundedDate;                
                LoanInfo.LoanNumber = LoanDetails.LoanInfo.Loan.ReferenceNumber;
                LoanInfo.MonthlyPaymentAmount = (int)LoanDetails.LoanInfo.Loan.Terms.PaymentAmount;

                // TODO: As of now, there is only "Personal Loan". There is no property in Loan model
                LoanInfo.ProductName = "Personal Loan";
                LoanInfo.TermInMonths = LoanDetails.LoanInfo.Loan.Terms.Term;
            }

            return LoanInfo;
        }
        #endregion
    }
}
