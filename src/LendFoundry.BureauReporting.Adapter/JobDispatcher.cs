﻿using LendFoundry.Configuration.Client;
using LendFoundry.Foundation.Date;
using LendFoundry.Foundation.Logging;
using LendFoundry.Security.Tokens;
using LendFoundry.Tenant.Client;
using Quartz;
using Quartz.Impl;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Linq;
using LendFoundry.BureauReporting.Adapter.Abstractions;
using LendFoundry.BureauReporting.Adapter.Abstractions.Configuration;

namespace LendFoundry.BureauReporting.Adapter
{
    public class JobDispatcher : IJobDispatcher
    {
        private ILogger Logger { get; }
        private ITenantServiceFactory TenantServiceFactory { get; }
        private ITokenHandler TokenHandler { get; }
        private IServiceProvider ServiceProvider { get; }
        private ITenantTimeFactory TenantTimeFactory { get; }
        private IConfigurationServiceFactory ConfigurationFactory { get; }
        private IDictionary<string, object> CreateJobDataMap(StaticTokenReader tenantReader) =>
            new Dictionary<string, object>
                {
                    { "provider", ServiceProvider },
                    { "tenantReader", tenantReader }
                };
        private IJobDetail CreateJob<T>(IDictionary<string, object> jobDataMap)
            where T : IJob => JobBuilder
                                .Create<T>()
                                .SetJobData(new JobDataMap(jobDataMap))
                                .Build();
        private ITrigger CreateTrigger(TenantInfo tenant, StaticTokenReader reader)
        {
            var configuration = ConfigurationFactory.Create<AdapterConfiguration>(Settings.ServiceName, reader).Get();

            if (configuration == null)
                throw new ArgumentException("No configuration found to start the application");

            if (configuration.Events == null)
                throw new ArgumentException("No triggers found in configuration");

            var tenantTime = TenantTimeFactory.Create(ConfigurationFactory, reader);

            var configKey = new ConfigurationKeys { ReportingDateDayOfMonth = configuration.ReportingDateDayOfMonth };

            //if (configKey == null) return null;
            configKey.TenantDateTime = tenantTime.Now;

            var triggerBuilder = TriggerBuilder.Create();

            var expression = new CronExpression(configKey.GetCronExpression()) {  TimeZone = tenantTime.TimeZone };

            var schedule = CronScheduleBuilder.CronSchedule(expression);

            triggerBuilder = triggerBuilder.WithSchedule(schedule);

            return triggerBuilder.StartNow().Build();
        }

        public JobDispatcher
        (
            ILogger logger,
            ITenantServiceFactory tenantServiceFactory,
            ITokenHandler tokenHandler,
            IServiceProvider provider,
            ITenantTimeFactory tenantTimeFactory,
            IConfigurationServiceFactory configurationFactory
        )
        {
            Logger = logger;
            TenantServiceFactory = tenantServiceFactory;
            TokenHandler = tokenHandler;
            ServiceProvider = provider;
            TenantTimeFactory = tenantTimeFactory;
            ConfigurationFactory = configurationFactory;
        }        

        public void Start()
        {
            // Start job scheduler.
            var scheduler = new StdSchedulerFactory().GetScheduler();
            scheduler.Start();

            // Process all active tenants.
            TenantServiceFactory.FetchActiveTenants(TokenHandler, (tenant, reader) =>
            {
                // Set tenant tasks.
                var dataMap = CreateJobDataMap(reader);
                scheduler.Attach
                (
                    new KeyValuePair<ITrigger, IJobDetail>(CreateTrigger(tenant, reader), CreateJob<Jobs.ScheduleUploadLoanDataJob>(dataMap))
                );

                Logger.Info($"Tasks attached to tenant {tenant.Id}");
            });
        }
    }

    public static class StatusManagementExtensions
    {
        public static void FetchParallelActiveTenants(this ITenantServiceFactory factory, ITokenHandler tokenHandler, Action<TenantInfo, StaticTokenReader> @action)
        {
            var tenantService = factory.Create(new StaticTokenReader(string.Empty));
            Parallel.ForEach(tenantService.GetActiveTenants(), tenant =>
            {
                var token = tokenHandler.Issue(tenant.Id, Settings.ServiceName);
                var reader = new StaticTokenReader(token.Value);
                @action.Invoke(tenant, reader);
            });
        }

        public static void FetchActiveTenants(this ITenantServiceFactory factory, ITokenHandler tokenHandler, Action<TenantInfo, StaticTokenReader> @action)
        {
            var tenantService = factory.Create(new StaticTokenReader(string.Empty));
            tenantService.GetActiveTenants().ForEach(tenant =>
            {
                var token = tokenHandler.Issue(tenant.Id, Settings.ServiceName);
                var reader = new StaticTokenReader(token.Value);
                @action.Invoke(tenant, reader);
            });
        }

        public static void Attach(this IScheduler scheduler, params KeyValuePair<ITrigger, IJobDetail>[] jobs)
            => jobs.ToList().ForEach(j => scheduler.ScheduleJob(j.Value, j.Key));
    }
}

