﻿using LendFoundry.BureauReporting.Abstractions.Services;
using LendFoundry.BureauReporting.Adapter.Abstractions;
using LendFoundry.EventHub.Client;
using LendFoundry.Foundation.Logging;
using LendFoundry.Loans.Client;
using LendFoundry.Notifications.Client;

namespace LendFoundry.BureauReporting.Adapter
{
    public interface IScheduleExecutorFactory
    {
        //1. LendFoundry.BureauReporting.Client (IReportDataService)
        //2. LendFoundry.BureauReporting.Adapter.Persistence (IAdapterRepository)
        //3. Schedule (ConfigurationKeys)
        //4. EventHub (IEventHubClient)
        //5. INotificationService
        //6. IAdapterService
        //7. ILoanService
        IScheduleExecutor Create
        (
            IReportDataService bureauReportingService,
            ILogger logger,
            IAdapterRepository repository,
            IEventHubClient eventHub,
            INotificationService notificationService,
            IAdapterService adapterService,
            ILoanService loanService
        );
    }
}
