﻿using LendFoundry.Configuration.Client;
using LendFoundry.EventHub.Client;
using LendFoundry.Foundation.Date;
using LendFoundry.Foundation.Services;
using LendFoundry.Foundation.Logging;
using System;
using System.Collections.Generic;
using LendFoundry.BureauReporting.Adapter.Abstractions;
using LendFoundry.BureauReporting.Adapter.Abstractions.Configuration;
using LendFoundry.Loans.Client;
using LendFoundry.Loans;
using LendFoundry.Tenant.Client;
using LendFoundry.BureauReporting.Abstractions.Requests.Payments;

namespace LendFoundry.BureauReporting.Adapter
{
    public class AdapterService : IAdapterServiceExtended
    {
        #region Private Properties
        private IAdapterRepository Repository { get; }
        private ILogger Logger { get; }
        private IAdapterEntry Entry { get; set; }
        private IConfigurationService<AdapterConfiguration> ConfigurationService { get; }
        private ILoanDetails LoanDetails { get; set; }
        private TimeBucket SnapshotTime { get; set; }
        private IEventHubClient EventHubClient { get; set; }
        private IAdapterEntry Model { get; set; }
        private TenantInfo TenantInfo { get; set; }
        private DateTimeOffset CutOffPeriod { get; set; }
        #endregion

        #region Constructor
        /// <summary>
        /// Getting all the injections and storing in private properties
        /// </summary>
        /// <param name="repository"></param>
        /// <param name="logger"></param>
        /// <param name="configurationService"></param>
        /// <param name="tenantInfo"></param>
        /// <param name="eventHubClient"></param>
        public AdapterService(IAdapterRepository repository,
            ILogger logger,
            IConfigurationService<AdapterConfiguration> configurationService,
            TenantInfo tenantInfo,
            IEventHubClient eventHubClient)
        {
            Repository = repository;
            Logger = logger;
            ConfigurationService = configurationService;
            TenantInfo = tenantInfo;
            EventHubClient = eventHubClient;
        }
        #endregion

        #region Public Methods
        public IEnumerable<IAdapterEntry> GetCurrentPeriodData()
        {
            return Repository.GetCurrentPeriodData();
        }

        public IAdapterEntry GetBureauReportData(string loanReferenceNumber)
        {
            return Repository.GetBureauReportData(loanReferenceNumber);
        }

        public Action<EventInfo> ProcessEvent(EventMapping eventMapping, String reportingDateDayOfMonth, ILoanService loanService, ILogger logger)
        {
            return @event =>
            {
                try
                {
                    var loanReferenceNumber = eventMapping.LoanReferenceNumber.FormatWith(@event);

                    if (string.IsNullOrWhiteSpace(loanReferenceNumber))
                    {
                        throw new ArgumentException($"{nameof(loanReferenceNumber)} can't be null");
                    }
                    else if (string.IsNullOrWhiteSpace(reportingDateDayOfMonth))
                    {
                        throw new ArgumentException($"{nameof(reportingDateDayOfMonth)} can't be null, please check your configuration");
                    }

                    logger.Info($"Received #{@event.Name} from #{@event.TenantId}");

                    var loanDetails = loanService.GetLoanDetails(loanReferenceNumber);
                    if (loanDetails != null)
                    {
                        SetCutOffPeriod(reportingDateDayOfMonth, @event.Time);
                        AddOrUpdate(loanDetails, @event.Time, @event.Name);
                        logger.Info($"Loan #{loanReferenceNumber} recorded successfully");
                    }
                    else
                    {
                        logger.Warn($"Loan #{loanReferenceNumber} could not be found");
                    }
                }
                catch (Exception ex)
                {
                    logger.Error($"Error while processing event #{@event.Name}", ex);
                }
            };
        }
        #endregion

        #region SetCutOffPeriod
        /// <summary>
        /// Setting CutOffPeriod per configuration
        /// </summary>
        /// <param name="reportingDateDayOfMonth"></param>
        /// <param name="timeBucket"></param>
        private void SetCutOffPeriod(string reportingDateDayOfMonth, TimeBucket timeBucket)
        {
            DateTimeOffset dateTimeOffset;
            int dayOfMonth = -1;

            if (string.Equals(reportingDateDayOfMonth, BureauReporting.Abstractions.Configuration.Common.LastDayOfMonth, StringComparison.OrdinalIgnoreCase))
            {
                dayOfMonth = DateTime.DaysInMonth(timeBucket.Time.Year, timeBucket.Time.Month);
            }
            else
            {
                int.TryParse(reportingDateDayOfMonth, out dayOfMonth);
            }

            dateTimeOffset = new DateTimeOffset(new DateTime(timeBucket.Time.Year, timeBucket.Time.Month, dayOfMonth), timeBucket.Time.Offset);

            if (dateTimeOffset > timeBucket.Time.Date)
            {
                CutOffPeriod = dateTimeOffset;
            }
            else
            {
                CutOffPeriod = dateTimeOffset.AddMonths(1);
            }
        }
        #endregion

        #region AddOrUpdate
        /// <summary>
        /// Add or Update data
        /// </summary>
        /// <param name="loanDetails">Loan Data</param>
        /// <param name="snapshotTime">Snapshot Time</param>
        private void AddOrUpdate(ILoanDetails loanDetails, TimeBucket snapshotTime, string EventName)
        {
            if (loanDetails == null || loanDetails.LoanInfo == null || loanDetails.LoanInfo.Loan == null)
            {
                throw new ArgumentException($"{nameof(loanDetails)} cannot be null");
            }
            else if (string.IsNullOrWhiteSpace(loanDetails.LoanInfo.Loan.ReferenceNumber))
            {
                throw new ArgumentException("Loan must have a LoanReferenceNumber");
            }

            LoanDetails = loanDetails;
            SnapshotTime = snapshotTime;
            string logInfo = string.Empty;
            string log = string.Empty;

            if (CanSkip(LoanDetails.LoanInfo.Loan.Status.Code))
            {
                logInfo = $"Loan data loan # {loanDetails.LoanInfo.Loan.ReferenceNumber} skipped due to Loan.Status.Code {LoanDetails.LoanInfo.Loan.Status.Code}";
                log = "{ 'type': 'skiped', 'output': 'Loan data is not added due to status code (LoanDetails.LoanInfo.Loan.Status.Code: " +
                    LoanDetails.LoanInfo.Loan.Status.Code + ", ReferenceNumber: " + loanDetails.LoanInfo.Loan.ReferenceNumber + ", SnapshotTime: " +
                    SnapshotTime.ToString() + ")' }";
                LogAndRaiseEventHubEvent(logInfo, log, "BureauReportingAdapter-QueueData-Skipped");

                FetchReportData();
                Repository.AddOrUpdate(Model);
            }
            else
            {
                FetchReportData();

                switch (EventName.ToLower())
                {
                    case "loanonboarded":
                        LoanOnboarded();
                        break;
                    default:
                        ManageQueueData(EventName);
                        break;
                }


                /*temp code*/

            }
        }
        #endregion

        #region FetchReportData
        /// <summary>
        /// Fetching report data
        /// 1. For current period (Year and Month)
        /// 2. If data doesn't exists, need to fetch for previous month
        /// </summary>
        private void FetchReportData()
        {
            Model = Repository.GetBureauReportData(LoanDetails.LoanInfo.Loan.ReferenceNumber, CutOffPeriod);

            if (Model != null)
            {
                Model.IsSkipped = CanSkip(LoanDetails.LoanInfo.Loan.Status.Code);
            }
        }
        #endregion

        #region LoanOnboarded
        /// <summary>
        /// Following actions are performing
        /// 1. Mapping LoanData to BureauReportData
        /// 2. Handling if BureauReportData exists
        /// 3. Updating|Saving BureauReportData queue
        /// </summary>
        private void LoanOnboarded()
        {
            MapLoanDataToBureauReportData();
            HandleReportDataPerExisting();
            Repository.AddOrUpdate(Model);

            string logInfo = logInfo = $"Loan data loan # {LoanDetails.LoanInfo.Loan.ReferenceNumber} added in queue for bureau-reporting";
            string log = log = "{ 'type': 'success', 'output': 'Loan data is added (ReferenceNumber: " + LoanDetails.LoanInfo.Loan.ReferenceNumber +
                ", SnapshotTime: " + SnapshotTime.Time.ToString() + ")' }";
            LogAndRaiseEventHubEvent(logInfo, log, "BureauReportingAdapter-QueueData-Success");
        }
        #endregion        

        #region ManageQueueData
        /// <summary>
        /// Following actions are performing
        /// 1. Updating AccountInfo
        /// 2. Updating Borrowers
        /// 3. Updating PaymentHistory
        /// 4. Updating Queue
        /// </summary>
        private void ManageQueueData(string EventName)
        {
            string logInfo = string.Empty;
            string log = string.Empty;
            string eventName = string.Empty;

            if (Model == null)
            {
                MapLoanDataToBureauReportData();
                HandleReportDataPerExisting();
            }
            else
            {
                Model.AccountInfo = new BureauReportAccountInfo(LoanDetails).MapAccountInfo();
                Model.Borrowers = new BureauReportBorrowerInfo(LoanDetails).MapBorrowerInfo();
                Model.PaymentHistory = new BureauReportPaymentHistory(LoanDetails, Model.CutOffPeriod).MapPaymentHistory();
            }

            Repository.AddOrUpdate(Model);

            logInfo = $"Per loan # {LoanDetails.LoanInfo.Loan.ReferenceNumber}, data updated bureau-reporting queue.";
            log = "{ 'type': 'success', 'output': 'Per loan # (ReferenceNumber: " + LoanDetails.LoanInfo.Loan.ReferenceNumber +
                ", data updated bureau-reporting queue, SnapshotTime: " + SnapshotTime.Time.ToString() + ")' }";
            eventName = "BureauReportingAdapter-" + EventName + "-Success";

            LogAndRaiseEventHubEvent(logInfo, log, eventName);
        }
        #endregion

        #region MapLoanDataToBureauReportData
        /// <summary>
        /// Map Loan data to Bureau report data
        /// 1. LoanInfo
        /// 2. AccountInfo
        /// 3. Borrowers
        /// 4. PaymentHistory
        /// </summary>
        private void MapLoanDataToBureauReportData()
        {
            Entry = new AdapterEntry();
            Entry.CutOffPeriod = CutOffPeriod;
            Entry.TenantId = TenantInfo.Id;
            Entry.IsSkipped = CanSkip(LoanDetails.LoanInfo.Loan.Status.Code);

            MapLoanInfoData();
            MapAccountInfoData();
            MapBorrowersData();
            MapPaymentHistoryData();
        }
        #endregion

        #region HandleReportDataPerExisting
        /// <summary>
        /// Handle ReportData per existing data
        /// 1. Update AccountInfo data per current month
        /// 2. Update Borrowers data per current month
        /// 3. Add PaymentHistory data for current month
        /// </summary>
        private void HandleReportDataPerExisting()
        {
            if (Model != null)
            {
                Entry.Id = Model.Id;
                Model.AccountInfo = Entry.AccountInfo;
                Model.Borrowers = Entry.Borrowers;
                if (Model.PaymentHistory == null)
                {
                    Model.PaymentHistory = new List<PaymentDetailsRequestData>();
                }
            }
            else
            {
                Model = Entry;
            }
        }
        #endregion

        #region MapLoanInfoData
        /// <summary>
        /// Map to LoanInfo object
        /// </summary>
        private void MapLoanInfoData()
        {
            Entry.LoanInfo = new BureauReportLoanInfo(LoanDetails).MapLoanInfo();
        }
        #endregion

        #region MapAccountInfoData
        /// <summary>
        /// Map to AccountInfo object
        /// </summary>
        private void MapAccountInfoData()
        {
            Entry.AccountInfo = new BureauReportAccountInfo(LoanDetails).MapAccountInfo();
        }
        #endregion

        #region MapBorrowersData
        /// <summary>
        /// Map to BorrowerInfo object
        /// </summary>
        private void MapBorrowersData()
        {
            Entry.Borrowers = new BureauReportBorrowerInfo(LoanDetails).MapBorrowerInfo();
        }
        #endregion

        #region MapPaymentHistoryData
        /// <summary>
        /// Map to PaymentHistory object
        /// </summary>
        private void MapPaymentHistoryData()
        {
            Entry.PaymentHistory = new BureauReportPaymentHistory(LoanDetails, Entry.CutOffPeriod).MapPaymentHistory();
        }
        #endregion

        #region CanSkip
        /// <summary>
        /// ### Skipping following code
        /// 100.09 - Bankruptcy
        /// 200.01 - Legal
        /// 200.02 - Research
        /// 200.03 - Fraud
        /// 200.04 - Other Issues
        /// 200.05 - Incarcerated
        /// 200.06 - Cease & Desist
        /// 300.01 - Charged Off
        /// 300.02 - Deceased
        /// 300.03 - Bankruptcy - PaidOff
        /// 300.04 - Bankruptcy - Discharged
        /// 300.06 - Charged Off - Fraud
        /// ------------------------------
        /// ### Considering following code
        /// 100.01 - OnBoarding
        /// 100.02 - OnBoarded
        /// 100.03 - InService
        /// 100.04 - Delinquent
        /// 100.05 - Collections
        /// 100.06 - SkipTrace
        /// 100.07 - Forbearance
        /// 100.08 - CivilReliefAct
        /// 300.05 - PaidOff
        /// </summary>
        /// <returns>bool</returns>
        public bool CanSkip(string Code)
        {
            switch (Code)
            {
                case "100.01":
                case "100.02":
                case "100.03":
                case "100.04":
                case "100.05":
                case "100.06":
                case "100.07":
                case "100.08":
                case "300.05":
                    return false;
                default:
                    return true;
            }
        }
        #endregion

        #region LogAndRaiseEventHubEvent
        public void LogAndRaiseEventHubEvent(string LogInfo, string log, string EventName)
        {
            Logger.Info(LogInfo);
            EventHubClient.Publish(EventName, log);
        }
        #endregion
    }
}
