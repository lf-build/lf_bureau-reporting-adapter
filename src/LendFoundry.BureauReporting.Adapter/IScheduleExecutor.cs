﻿using System;

namespace LendFoundry.BureauReporting.Adapter
{
    public interface IScheduleExecutor
    {
        void RunIt(DateTimeOffset schedule);
    }
}
