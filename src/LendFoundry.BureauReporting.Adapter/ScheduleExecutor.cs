﻿using LendFoundry.BureauReporting.Abstractions.Requests;
using LendFoundry.BureauReporting.Abstractions.Services;
using LendFoundry.BureauReporting.Adapter.Abstractions;
using LendFoundry.EventHub.Client;
using LendFoundry.Foundation.Logging;
using LendFoundry.Loans;
using LendFoundry.Loans.Client;
using LendFoundry.Notifications.Client;
using System;
using System.Collections.Generic;

namespace LendFoundry.BureauReporting.Adapter
{
    public class ScheduleExecutor : IScheduleExecutor
    {
        #region Private Properties
        private IReportDataService BureauReportingService { get; }
        private ILogger Logger { get; }
        private IAdapterRepository Repository { get; }
        private IEventHubClient EventHub { get; }
        private INotificationService NotificationService { get; }
        private IAdapterService AdapterService { get; }
        private ILoanService LoanService { get; }
        #endregion

        #region Private Methods
        private ICollection<ReportDataRequest> ConvertToReportDataRequest(IEnumerable<IAdapterEntry> QueueData)
        {
            var reportDataRequest = new List<ReportDataRequest>();

            foreach (var queueData in QueueData)
            {
                reportDataRequest.Add(new ReportDataRequest
                {
                    AccountInfo = queueData.AccountInfo,
                    Borrowers = queueData.Borrowers,
                    LoanInfo = queueData.LoanInfo,
                    PaymentHistory = queueData.PaymentHistory
                });
            }

            return reportDataRequest;
        }
        #endregion

        #region Constructor
        public ScheduleExecutor
        (
            IReportDataService bureauReportingService,
            ILogger logger,
            IAdapterRepository repository,
            IEventHubClient eventHub,
            INotificationService notificationService,
            IAdapterService adapterService,
            ILoanService loanService
        )
        {
            BureauReportingService = bureauReportingService;
            Logger = logger;
            EventHub = eventHub;
            Repository = repository;
            NotificationService = notificationService;
            AdapterService = adapterService;
            LoanService = loanService;
        }
        #endregion

        public void RunIt(DateTimeOffset schedule)
        {
            string logInfo = string.Empty;
            string log = string.Empty;
            bool canAdd = false;
            try
            {
                // Fetch missing data into queue
                var MissingQueueData = Repository.ScanMissingData(schedule);

                foreach (var Model in MissingQueueData)
                {
                    canAdd = true;
                    var loanDetails = GetLoanData(Model.LoanInfo.LoanNumber);
                    if (loanDetails != null)
                    {
                        if (AdapterService.CanSkip(loanDetails.LoanInfo.Loan.Status.Code))
                        {
                            logInfo = $"Loan data loan # {loanDetails.LoanInfo.Loan.ReferenceNumber} skipped due to Loan.Status.Code {loanDetails.LoanInfo.Loan.Status.Code}";
                            log = "{ 'type': 'skiped', 'output': 'Loan data is not added due to status code (LoanDetails.LoanInfo.Loan.Status.Code: " +
                                loanDetails.LoanInfo.Loan.Status.Code + ", ReferenceNumber: " + loanDetails.LoanInfo.Loan.ReferenceNumber + ", SnapshotTime: " +
                                schedule.ToString() + ")' }";
                            AdapterService.LogAndRaiseEventHubEvent(logInfo, log, "BureauReportingAdapter-QueueData-Skipped");

                            Model.IsSkipped = true;
                        }
                        else
                        {
                            Model.AccountInfo = new BureauReportAccountInfo(loanDetails).MapAccountInfo();
                            Model.Borrowers = new BureauReportBorrowerInfo(loanDetails).MapBorrowerInfo();
                            Model.PaymentHistory = new BureauReportPaymentHistory(loanDetails, Model.CutOffPeriod).MapPaymentHistory();
                        }
                    }
                }

                // Add missing data into queue
                if (canAdd)
                {
                    Repository.AddData(MissingQueueData, schedule);

                    logInfo = "Missing data has successfully added into queue data.";
                    log = "{ 'type': 'success', 'output': 'Missing data has successfully added into queue data, 'scheduleTime': '" + schedule.ToString() + "' }";
                    AdapterService.LogAndRaiseEventHubEvent(logInfo, log, "BureauReportingAdapter-MissingDataAdded-Successfully");
                }

                // Fetch queue data per current period
                var QueueData = Repository.GetCurrentPeriodData(false);

                // Call Bureau-Reporting Client to send the data
                var reportDataRequest = ConvertToReportDataRequest(QueueData);

                if (reportDataRequest == null || (reportDataRequest != null && reportDataRequest.Count == 0))
                {
                    logInfo = $"Queue data count is zero (data not found to upload to bureau-reporting api).";
                    log = "{ 'type': 'failed', 'output': 'Queue data count is zero (data not found to upload to bureau-reporting api), 'scheduleTime': '" + schedule.ToString() + "' }";
                    AdapterService.LogAndRaiseEventHubEvent(logInfo, log, "BureauReportingAdapter-QueueDataUploaded-Successfully");

                    return;
                }

                var message = BureauReportingService.AddBureauReportData(reportDataRequest, schedule.Date, NotificationService, false);

                if (message != null && message.ToString().ToLower() == "true")
                {
                    // Update queue data                
                    Repository.UpdateSendProcessToBureau(QueueData, schedule);

                    logInfo = $"Queue data has been sent to bureau-reporting at {schedule}. Queue data status has successfully updated.";
                    log = "{ 'type': 'success', 'output': 'Queue data has been sent to bureau-reporting api. Queue data status has successfully updated, 'scheduleTime': '" + schedule.ToString() + "' }";
                    AdapterService.LogAndRaiseEventHubEvent(logInfo, log, "BureauReportingAdapter-QueueDataUploaded-Successfully");

                    Logger.Info($"Task executed properly at {schedule}");
                }
                else
                {
                    logInfo = $"Queue data has not sent to bureau-reporting api at {schedule}. Queue data upload has failed to bureau-reporting api.";
                    log = "{ 'type': 'failed', 'output': 'Queue data has not sent to bureau-reporting api. Queue data upload has failed to bureau-reporting api, 'scheduleTime': '" +
                        schedule.ToString() + "', 'message': '" + message.ToString() + "' }";
                    AdapterService.LogAndRaiseEventHubEvent(logInfo, log, "BureauReportingAdapter-QueueDataUploaded-Successfully");
                }
            }
            catch (Exception ex)
            {
                logInfo = $"Error while executing at {schedule}, error {ex}";
                log = "{ 'type': 'error', 'output': 'Queue data upload failed to bureau-reporting api, 'scheduleTime': '" +
                    schedule.ToString() + "', 'errorMessage': '" + ex.Message + "', 'errorSource': '" + ex.Source + "', 'stackTrace':'" + ex.StackTrace + "' }";
                AdapterService.LogAndRaiseEventHubEvent(logInfo, log, "BureauReportingAdapter-QueueDataUploaded-Failed");
            }
        }

        /// <summary>
        /// Fetching Loan Data from loan service
        /// </summary>
        /// <param name="LoanNumber">LoanNumber</param>
        /// <returns>Loan object</returns>
        private ILoanDetails GetLoanData(string LoanNumber)
        {
            ILoanDetails loanData = null;
            try
            {
                loanData = LoanService.GetLoanDetails(LoanNumber);
            }
            catch(Exception ex)
            {
                Logger.Error($"Loan does not exists per loan # {LoanNumber}", ex.Message);
            }
            return loanData;
        }
    }
}
